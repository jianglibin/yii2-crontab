## 接入thinkphp6

> 如果此教程的原理搞懂了, 就按照正常的扩展文档开发即可

### 前提

1. thinkphp6以安装完毕
2. 进入thinkphp6根目录

## 示例教程

1. 安装本扩展

```composer require godv/yii2-crontab dev-master```

2. 创建yii调度文件(与tp6.0的```think```文件同级)

```php
 #!/usr/bin/env php
<?php
/**
 * Yii console bootstrap file.
 *
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

defined('YII_DEBUG') or define('YII_DEBUG', true);


require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/config/crontab.php';

$application = new yii\console\Application($config);
$exitCode = $application->run();
exit($exitCode);
 ```

3. 创建任务配置文件 (config/crontab.php)

```php
$config = [
    'id' => 'crontab-console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\commands',
    'timeZone' => 'Asia/Shanghai',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'crontab' => [ 
            'class' => 'CrontabConsole\controllers\CrontabController',
            'defaultScript' => 'think',
            'driver' => [
                'class' => 'CrontabConsole\drivers\File',
                'tasks' => [
                    ['crontab_str' => '* * * * *', 'route' => 'example minute'],
                    ['crontab_str' => '0 */1 * * *', 'route' => 'example hours'],
                ]

                // 'class' => 'CrontabConsole\drivers\Mysql',
                // 'dsn' => 'mysql:host=localhost;dbname=test',
                // 'username' => 'root',
                // 'password' => 'root',
                // 'charset' => 'utf8',
            ],
        ],
    ],

];
return $config;
 ```

4. 根据配置的任务, 创建对应TP的command文件

```php
['crontab_str' => '* * * * *', 'route' => 'example minute'],
['crontab_str' => '0 */1 * * *', 'route' => 'example hours'],
 ```

```application/Command/Example.php```

> tp6貌似不支持,一个主command文件,下面有多个子路由的方式执行命令, 所以改造了一下


**注意事项**

任务运行完必须用```exit()```函数退出, 这样任务调度可以知道任务是否正常运行,是不是报错了之类的.

exit(0) 表示正常退出, 其他表示不正常

```php
<?php
namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\console\input\Argument;
class Example extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('example')
            ->addArgument('action', Argument::OPTIONAL);
    }
    protected function execute(Input $input, Output $output)
    {
        $args = $input->getArguments('action');
        switch($args['action']) {
            case 'minute':
                exit($this->minute());
                break;
            case 'hours':
                exit($this->hours());
                break;
        }
    }
    /**
     * 每分钟运行
     */
    private function minute()
    {
        echo "run php think example minute","\n";
        // 编写你的业务代码吧!!
        return 0;
    }
    /**
     * 每小时运行
     */
    private function hours()
    {
        echo "run php think example hours","\n";
        // 编写你的业务代码吧!!
        return 0;
    }
}
 ```

5. 接入完毕, 开始运行吧

```
cmd>php yii crontab/drun

run php think example minute
.----------------.-------------.------------------.------------------.--------.------------.--------------.
|     route      | crontab_str |   last_rundate   |   next_rundate   | status | exec_count | exec_time(s) |
:----------------+-------------+------------------+------------------+--------+------------+--------------:
| example minute | * * * * *   | 2020-10-30 18:17 | 2020-10-30 18:18 | --     | 3          | 0.49         |
| example hours  | 0 */1 * * * | 2020-10-30 18:17 | 2020-10-30 19:00 | --     | 1          | 0.55         |
'----------------'-------------'------------------'------------------'--------'------------'--------------'
```

