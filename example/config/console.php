<?php

$config = [
    'id' => 'crontab-console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\commands',
    'timeZone' => 'Asia/Shanghai',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'crontab' => [ 
            'class' => 'CrontabConsole\controllers\CrontabController',
            'defaultScript' => 'yii',
            'driver' => [
                'class' => 'CrontabConsole\drivers\File',
                'tasks' => [
                    ['crontab_str' => '* * * * *', 'route' => 'example/minute'],
                    ['crontab_str' => '0 */1 * * *', 'route' => 'example/hours'],
                ]

                // 'class' => 'CrontabConsole\drivers\Mysql',
                // 'dsn' => 'mysql:host=localhost;dbname=test',
                // 'username' => 'root',
                // 'password' => 'root',
                // 'charset' => 'utf8',
            ],
        ],
    ],

];


return $config;