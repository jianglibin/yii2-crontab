<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * 定时任务调度控制器
 * @author jlb
 */
class ExampleController extends Controller {
    /**
     * 测试每分钟运行
     */
    public function actionMinute() {
        echo 'run example/minute 每分钟运行一次',"\n";
        return ExitCode::OK;
    }

    /**
     * 测试每小时运行
     */
    public function actionHours() {
        echo 'run example/hours 每小时运行一次',"\n";
        return ExitCode::OK;
    }
}