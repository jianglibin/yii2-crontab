<?php
require __DIR__ . '/../vendor/autoload.php';

use CrontabConsole\CronParser;

/**
 * 注意!!!!
 * 此脚本只建议在开发环境或者windows环境下调试使用!!!!
 * 生产环境请在linux下的crontab -e , 然后输入: * * * * * php yii crontab/run
 */

function crontab($cmd) {
    $result = proc_open($cmd, [], $pipe);
    while (1) {
        $etat = proc_get_status($result);
        if ($etat['running'] === false) {
            proc_close($result);
            break;
        }
    }
}

$nextRundatetime = CronParser::formatToDate('* * * * *')[0];
$cmd = 'php yii crontab/drun';

echo "cmd: $cmd, next_datetime: $nextRundatetime\n";

while(1) {
    if (date('Y-m-d H:i') >= $nextRundatetime) {
        crontab($cmd);
        $nextRundatetime = CronParser::formatToDate('* * * * *')[0];
        echo "cmd: $cmd, next_datetime: $nextRundatetime\n";
    }
    sleep(1);
}