这是一个基于yii2实现的定时任务管理工具, 不仅仅只支持yii2, 也可独立作为一个组件或者微框架使用。

## 特性

* 支持windows下开发调试,linux生产部署
* 任务状态监控与web可视化管理
* 服务器宕机, 定时任务自动重启
* 支持嵌入所有主流php框架
* 多种驱动类型, 满足你的单机或者多机部署需求: 文件(file), mysql
* crontab语法兼容90%, 灵活精准控制任务时间
* 时间粒度为分钟级别

## 依赖

* php5.6+ php7+
* ```proc_open``` 函数
* 生产环境依赖linux的crontab
* windows下需要模拟crontab调度器, 有提供(```faker-linux.php```)

## 关于windows下命令行输出乱码问题

控制台编码设置为utf8 [传送门](https://jingyan.baidu.com/article/d7130635e8a38413fdf4753b.html)


## 安装

一, 组件式引入使用

```composer require godv/yii2-crontab dev-master```

二, 独立项目基础版(开箱即用)

```composer create-project godv/yii2-crontab-basic crontab-basic``` [传送门](https://gitee.com/jianglibin/yii2-crontab-basic)

## crontab

在linux下crontab服务中添加

```
* * * * * /path/yii2-crontab/yii crontab/run
```

## 文档

+ [thinkphp6接入文档](https://gitee.com/jianglibin/yii2-crontab/blob/master/docs/thinkphp6.md)

## 快速一览

### 配置

```config/console.php```

```php
<?php
    'controllerMap' => [
        'crontab' => [ 
            'class' => 'CrontabConsole\controllers\CrontabController',
            'driver' => [
                'class' => 'CrontabConsole\drivers\File',
                'tasks' => [
                    ['crontab_str' => '* * * * *', 'route' => 'example/minute'],
                    ['crontab_str' => '0 */1 * * *', 'route' => 'example/hours'],
                ]

                // 'class' => 'CrontabConsole\drivers\Mysql',
                // 'dsn' => 'mysql:host=localhost;dbname=test',
                // 'username' => 'root',
                // 'password' => 'root',
                // 'charset' => 'utf8',
            ],
        ],
    ],
```
### 控制器

```commands/ExampleController.php```

```php
<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * 定时任务调度控制器
 */
class ExampleController extends Controller {
    /**
     * 测试每分钟运行
     */
    public function actionMinute() {
        echo 'run example/minute 每分钟运行一次',"\n";
        return ExitCode::OK;
    }

    /**
     * 测试每小时运行
     */
    public function actionHours() {
        echo 'run example/hours 每小时运行一次',"\n";
        return ExitCode::OK;
    }
}
```

```
php yii crontab/drun
.----------------.-------------.--------------.------------------.--------.------------.--------------.
|     route      | crontab_str | last_rundate |   next_rundate   | status | exec_count | exec_time(s) |
:----------------+-------------+--------------+------------------+--------+------------+--------------:
| example/minute | * * * * *   |              | 2020-10-26 17:56 | --     | 0          | 0            |
| example/hours  | 0 */1 * * * |              | 2020-10-26 18:00 | --     | 0          | 0            |
'----------------'-------------'--------------'------------------'--------'------------'--------------'
```

## 命令列表

+ [crontab/run](#crontab/run) 运行任务
+ [crontab/drun](#crontab/drun) 调试模式运行
+ [crontab/status](#crontab/status) 任务状态监控
+ [crontab/add](#crontab/add) 新增任务
+ [crontab/del](#crontab/del) 删除任务
+ [crontab/refrsh](#crontab/refrsh) 刷新任务配置与状态

## 命令兼容性

|  | run | drun | add | del |refrsh |
| :-----| :---- | :---- |:---- |:---- |:---- |
| File | √ | √ |×|×|√
| Mysql | √ | √ |√|√|×


## crontab/run

```
WWW\github\yii2-crontab>php yii crontab/run
```

```
Directory of WWW\github\yii2-crontab\example\runtime\crontab
2020/10/26  18:06                 0 2020-10-26-error.log --任务错误记录
2020/10/26  18:06                41 2020-10-26-output.log --任务输出记录
2020/10/26  18:06               631 crontab.data --用于存储任务运行状态
```

> 用此命令运行定时任务将会把控制台的输出重定向

* ```runtime/crontab/xxxx-xx-xx-error.log``` 错误日志
* ```runtime/crontab/xxxx-xx-xx-output.log``` 任务输出日志
* ```runtime/crontab/crontab.data``` 任务状态记录(仅限File模式下)

## crontab/drun

```
WWW\github\yii2-crontab\example>php yii crontab/drun
run example/minute 每分钟运行一次
.----------------.-------------.------------------.------------------.--------.------------.--------------.
|     route      | crontab_str |   last_rundate   |   next_rundate   | status | exec_count | exec_time(s) |
:----------------+-------------+------------------+------------------+--------+------------+--------------:
| example/minute | * * * * *   | 2020-10-26 18:15 | 2020-10-26 18:16 | --     | 2          | 0.43         |
| example/hours  | 0 */1 * * * | 2020-10-26 18:06 | 2020-10-26 19:00 | --     | 1          | 0.57         |
'----------------'-------------'------------------'------------------'--------'------------'--------------'
```
## crontab/status

```
WWW\github\yii2-crontab\example>php yii crontab/status
.----------------.-------------.------------------.------------------.--------.------------.--------------.
|     route      | crontab_str |   last_rundate   |   next_rundate   | status | exec_count | exec_time(s) |
:----------------+-------------+------------------+------------------+--------+------------+--------------:
| example/minute | * * * * *   | 2020-10-26 18:15 | 2020-10-26 18:16 | --     | 2          | 0.43         |
| example/hours  | 0 */1 * * * | 2020-10-26 18:06 | 2020-10-26 19:00 | --     | 1          | 0.57         |
'----------------'-------------'------------------'------------------'--------'------------'--------------'
```

## crontab/add

> 仅限Mysql驱动下

```
WWW\github\yii2-crontab\example>php yii crontab/add "route=example/test-add,crontab_str=* * * * *"
.----.------------------.-------------.---------------------.---------------------.---------.------------.--------------.
| id |      route       | crontab_str |    last_rundate     |    next_rundate     | status  | exec_count | exec_time(s) |
:----+------------------+-------------+---------------------+---------------------+---------+------------+--------------:
| 14 | example/hours    | * * * * *   | 2020-10-26 14:36:00 | 2020-10-26 14:37:00 | RUNNING | 19         | 0.36         |
| 20 | example/test-add | * * * * *   |                     | 2020-10-27 10:17:00 | --      | 0          | 0.00         |
| 13 | example/minute   | 35 14 * * * | 2020-10-26 14:35:00 | 2020-10-27 14:35:00 | --      | 20         | 0.80         |
'----'------------------'-------------'---------------------'---------------------'---------'------------'--------------'
```

## crontab/del

> 仅限Mysql驱动下

```
WWW\github\yii2-crontab\example>php yii crontab/del 20
.----.----------------.-------------.---------------------.---------------------.---------.------------.--------------.
| id |     route      | crontab_str |    last_rundate     |    next_rundate     | status  | exec_count | exec_time(s) |
:----+----------------+-------------+---------------------+---------------------+---------+------------+--------------:
| 14 | example/hours  | * * * * *   | 2020-10-26 14:36:00 | 2020-10-26 14:37:00 | RUNNING | 19         | 0.36         |
| 13 | example/minute | 35 14 * * * | 2020-10-26 14:35:00 | 2020-10-27 14:35:00 | --      | 20         | 0.80         |
'----'----------------'-------------'---------------------'---------------------'---------'------------'--------------'
```

## crontab/refresh

> 仅限File驱动下

```
WWW\github\yii2-crontab\example>php yii crontab/refresh
refresh success!
.----------------.-------------.--------------.------------------.--------.------------.--------------.
|     route      | crontab_str | last_rundate |   next_rundate   | status | exec_count | exec_time(s) |
:----------------+-------------+--------------+------------------+--------+------------+--------------:
| example/minute | * * * * *   |              | 2020-10-27 10:22 | --     | 0          | 0            |
| example/hours  | 0 */1 * * * |              | 2020-10-27 11:00 | --     | 0          | 0            |
'----------------'-------------'--------------'------------------'--------'------------'--------------'
```

## 关于web可视化管理

Mysql驱动运行任务,会在数据库里创建一个表: console_crontab, 基于这个表写个界面增删改查,应该很简单吧。 目前不知道这块的需求，定个小目标star200+,就新弄个组件或者集成进```godv/yii2-crontab-basic```扩展里

```php
CREATE TABLE `console_crontab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '任务名称',
  `route` varchar(255) NOT NULL COMMENT '任务路由',
  `count` int(11) DEFAULT NULL COMMENT '任务运行累积次数',
  `crontab_str` varchar(255) NOT NULL COMMENT 'crontab解析符',
  `switch` tinyint(1) DEFAULT NULL COMMENT '开关: 0-close 1-open',
  `status` tinyint(1) DEFAULT NULL COMMENT '任务状态: 0-正常 1-运行中 2-运行出错',
  `last_rundate` datetime DEFAULT NULL COMMENT '上次任务运行时间',
  `next_rundate` datetime DEFAULT NULL COMMENT '下次任务运行时间',
  `exectime` decimal(9,2) DEFAULT NULL COMMENT '任务单次运行时长(秒)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8
```

