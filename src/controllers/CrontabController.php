<?php

namespace CrontabConsole\controllers;

use CrontabConsole\ProcWorker;
use Exception;
use Yii;
use yii\console\Controller;

/**
 * 定时任务命令
 * @author 497012571@qq.com
 */
class CrontabController extends Controller
{
    private $crontabDb = null;

    /**
     * 数据驱动
     */
    public $driver;
    
    /**
     * 默认命令行
     * yii,think
     */
    public $defaultScript = 'yii';

    /**
     * 初始化
     */
    public function init()
    {
        ProcWorker::setScript($this->defaultScript);

        $this->crontabDb = Yii::createObject(['class' => '\CrontabConsole\Crontab', 'driver' => $this->driver]);
    }

    /**
     * 执行定时任务
     * command: crontab/run
     */
    public function actionRun() 
    {
        $this->crontabDb->run();
    }
    
    /**
     * 可以打印输出,一般用于调试
     */
    public function actionDrun() 
    {   
        // 关闭输出重定向
        ProcWorker::$restd = false;

        $this->crontabDb->run();
        $this->crontabDb->display();
    }

    /**
     * 查看定时任务运行状态
     * command: crontab/status
     */
    public function actionStatus() 
    {
        $this->crontabDb->display();
    }

    /**
     * 重新加载定时任务配置
     * command: crontab/refresh
     */
    public function actionRefresh() 
    {
        $this->crontabDb->refresh();
    }

    /**
     * 新增定时任务: 用于向数据库新增数据
     * command: crontab/add  
     * @example 
     * 'route=xxxx/xxx,crontab_str=* * * * *'
     */
    public function actionAdd(string $str = '')
    {
        if (empty($str)) {
            exit("缺少参数, 参考示例\n \t新增任务 => php yii crontab/add 'route=任务路由,crontab_str=* * * * *'\n \t修改任务 => php yii crontab/add 'id=任务id,route=任务路由,crontab_str=* * * * *'");
        }

        $str = trim($str, ',');

        // 判断字段是否支持
        $expData = explode(',', $str);
        $data = [];

        foreach ($expData as $d) {
            $val = explode('=', $d);
            $val[0] = trim($val[0]);
            $val[1] = iconv('gbk', 'utf-8', trim($val[1]));
            $data[$val[0]] = $val[1];
        }

        try{
            $this->crontabDb->add($data);
            $this->crontabDb->display();
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }

    /**
     * 删除定时任务
     * command: crontab/del  
     */
    public function actionDel(string $id)
    {
        if (empty($id)) {
            exit("缺少参数, 参考示例\n \t删除任务 => php yii crontab/del 任务ID");
        }
        try{
            $this->crontabDb->del($id);
            $this->crontabDb->display();
        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }
    
}
