<?php

namespace CrontabConsole;

use Yii;
use yii\base\Component;
use CrontabConsole\ProcWorker;
use CrontabConsole\drivers\Exception;


/**
 * 任务调度
 */
class Crontab extends Component {

    public $driver;

    public function run() {
        $driverObj = Yii::createObject($this->driver);

        ProcWorker::$driver = $driverObj;
        ProcWorker::$tasks = $driverObj->getRunTasks();

        ProcWorker::run();
    }

    public function display() 
    {
        Yii::createObject($this->driver)->display();
    }

    /**
     * 重新加载配置
     */
    public function refresh() 
    {
        $driver = Yii::createObject($this->driver);
        $driver->refresh();

        echo "refresh success! \n";

        $driver->display();
    }

    public function add($data) 
    {
        $driver = Yii::createObject($this->driver);
        $data['class'] = '\CrontabConsole\Task';
        $task = Yii::createObject($data);
        $driver->add($task);
    }

    public function del($id) 
    {
        $driver = Yii::createObject($this->driver);
        $driver->del($id);
    }

}