<?php
namespace CrontabConsole\drivers;

use yii\base\UserException;

class Exception extends UserException
{
    public function getName()
    {
        return 'Error';
    }
}