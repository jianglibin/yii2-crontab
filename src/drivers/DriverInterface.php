<?php

namespace CrontabConsole\drivers;

/**
 * 驱动接口
 */
interface DriverInterface
{
    /**
     * 初始化数据
     */
    public function installCrontab();
    /**
     * 获取所有任务
     * @return Array [Task1, Task2...]
     */
    public function getAllTasks();
    /**
     * 获取所有当下可以运行的任务
     * @return Array [Task1, Task2...]
     */
    public function getRunTasks();

    /**
     * 新增任务
     */
    public function add(\CrontabConsole\Task $task);

    public function del($id);

    /**
     * 更新单个任务状态(上锁更新)
     * @return number
     */
    public function updateOne(\CrontabConsole\Task $task);

    /**
     * 重载任务配置
     */
    public function refresh();
    /**
     * 向控制台打印任务状态列表
     */
    public function display();
}